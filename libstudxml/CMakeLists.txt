add_subdirectory(details)

add_library(studxml
    parser.cxx
	qname.cxx
	serializer.cxx
	value-traits.cxx
	$<TARGET_OBJECTS:expat>
	$<TARGET_OBJECTS:genx>)

if(BUILD_SHARED_LIBS)
	message(STATUS "SHARED BUILD REQUESTED")
	target_compile_definitions(studxml PRIVATE -DLIBSTUDXML_SHARED_BUILD)
else(BUILD_SHARED_LIBS)
	message(STATUS "STATIC BUILD REQUESTED")
	target_compile_definitions(studxml PRIVATE -DLIBSTUDXML_STATIC_BUILD)
endif(BUILD_SHARED_LIBS)

install(FILES
	"content.hxx"
	"exception.hxx"
	"forward.hxx"
	"parser.hxx"
	"parser.ixx"
	"parser.txx"
	"qname.hxx"
	"serializer.hxx"
	"serializer.ixx"
	"value-traits.hxx"
	"value-traits.txx"
	"version.hxx"
	DESTINATION "include/libstudxml")

install(
	TARGETS studxml
	RUNTIME DESTINATION bin 
	ARCHIVE DESTINATION lib 
	LIBRARY DESTINATION lib)